package com.devcamp.pizza365backend.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365backend.Services.CDailyCampaignService;

@RestController
@CrossOrigin
public class CDailyCampaignController {
    @Autowired
    CDailyCampaignService cDailyCampaignService;

    @GetMapping("/campaigns")
    public String getDailyCampaign (){
        String dailyCampain = cDailyCampaignService.getDailyCampaignString();
        return dailyCampain;
    }
}
