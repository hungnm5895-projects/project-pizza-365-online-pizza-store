package com.devcamp.pizza365backend.Models;

public class CDrink {
    private String maNuocUong;
    private String tenNuocUong;
    private int donGia;
    private String ghiChu;
    private Long ngayTao;
    private Long ngayCapNhat;

    public CDrink(String maNuocUong, String tenNuocUong, int donGia, String ghiChu, Long ngayTao) {
        this.maNuocUong = maNuocUong;
        this.tenNuocUong = tenNuocUong;
        this.donGia = donGia;
        this.ghiChu = ghiChu;
        this.ngayTao = ngayTao;
    }

    public String getmaNuocUong() {
        return maNuocUong;
    }

    public void setmaNuocUong(String maNuocUong) {
        this.maNuocUong = maNuocUong;
    }

    public String gettenNuocUong() {
        return tenNuocUong;
    }

    public void settenNuocUong(String tenNuocUong) {
        this.tenNuocUong = tenNuocUong; 
    }

    public int getdonGia() {
        return donGia;
    }

    public void setdonGia(int donGia) {
        this.donGia = donGia;
    }

    public String getghiChu() {
        return ghiChu;
    }

    public void setghiChu(String ghiChu) {
        this.ghiChu = ghiChu;
    }

    public Long getngayTao() {
        return ngayTao;
    }

    public void setngayTao(Long ngayTao) {
        this.ngayTao = ngayTao;
    }

    public Long getngayCapNhat() {
        return ngayCapNhat;
    }

    public void setngayCapNhat(Long ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }

    @Override
    public String toString() {
        return "CDrink [maNuocUong=" + maNuocUong + ", tenNuocUong=" + tenNuocUong + ", donGia=" + donGia + ", ghiChu="
                + ghiChu + ", ngayTao=" + ngayTao + ", ngayCapNhat=" + ngayCapNhat + "]";
    }

    
}
