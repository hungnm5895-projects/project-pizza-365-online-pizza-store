package com.devcamp.pizza365backend.Services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.pizza365backend.Models.CMenu;

@Service
public class CMenuService {
    private CMenu menuS = new CMenu("S", "20 cm", "2", "200 g", "2", "170.000");
    private CMenu menuM = new CMenu("M", "25 cm", "4", "300 g", "3", "220.000");
    private CMenu menuL = new CMenu("L", "30 cm", "6", "500 g", "4", "270.000");

    public ArrayList<CMenu> getAllCombo() {
        ArrayList<CMenu> menuList = new ArrayList<>();
        menuList.add(menuS);
        menuList.add(menuM);
        menuList.add(menuL);
        return menuList;
    }
}
