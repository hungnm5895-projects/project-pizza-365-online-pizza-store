package com.devcamp.pizza365backend.Controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365backend.Models.CMenu;
import com.devcamp.pizza365backend.Services.CMenuService;

@RestController
@CrossOrigin
public class CMenuController {
    @Autowired
    CMenuService cMenuService;

    @GetMapping("/combomenu")
    public ArrayList<CMenu> getAllCombo(){
        ArrayList<CMenu> allCombo = cMenuService.getAllCombo();
        return allCombo;
    }
}
