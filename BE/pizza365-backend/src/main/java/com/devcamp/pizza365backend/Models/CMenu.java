package com.devcamp.pizza365backend.Models;

public class CMenu {
    private String kichCo;
    private String duongKinh;
    private String suongNuong;
    private String salad;
    private String nuocNgot;
    private String thanhTien;

    public CMenu(String kichCo, String duongKinh, String suongNuong, String salad, String nuocNgot, String thanhTien) {
        this.kichCo = kichCo;
        this.duongKinh = duongKinh;
        this.suongNuong = suongNuong;
        this.salad = salad;
        this.nuocNgot = nuocNgot;
        this.thanhTien = thanhTien;
    }

    public String getkichCo() {
        return kichCo;
    }

    public void setkichCo(String kichCo) {
        this.kichCo = kichCo;
    }

    public String getduongKinh() {
        return duongKinh;
    }

    public void setduongKinh(String duongKinh) {
        this.duongKinh = duongKinh;
    }

    public String getsuonNuong() {
        return suongNuong;
    }

    public void setsuonNuong(String suongNuong) {
        this.suongNuong = suongNuong;
    }

    public String getsalad() {
        return salad;
    }

    public void setsalad(String salad) {
        this.salad = salad;
    }

    public String getnuocNgot() {
        return nuocNgot;
    }

    public void setnuocNgot(String nuocNgot) {
        this.nuocNgot = nuocNgot;
    }

    public String getthanhTien() {
        return thanhTien;
    }

    public void setthanhTien(String thanhTien) {
        this.thanhTien = thanhTien;
    }

}
