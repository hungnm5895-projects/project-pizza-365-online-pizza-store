package com.devcamp.pizza365backend.Services;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.stereotype.Service;

@Service
public class CDailyCampaignService {
    
    //Service xử lý lấy thứ hiện tại trong tuần
    //Output: trả về String là campain hôm nay
    public String getDailyCampaignString() {
        // Lấy ngày hiện tại
        // LocalDateTime myDateObj = LocalDateTime.of(2023, 8, 6, 7, 5, 36); //Tùy chỉnh ngày để test

        LocalDateTime myDateObj = LocalDateTime.now();
        DateTimeFormatter myFormatObj1 = DateTimeFormatter.ofPattern("E"); //Lấy ra thứ hiện tại
        String thisDay = myDateObj.format(myFormatObj1);

        // So sánh thứ hiện tại để trả về kết quả tương ứng
        if (thisDay.equals("Mon")) {
            thisDay = "Hôm nay thứ hai, mua 1 tặng 1!";
        } else if (thisDay.equals("Tue")) {
            thisDay = "Hôm nay thứ ba, tặng tất cả khách hàng một phần bánh ngọt!";
        } else if (thisDay.equals("Web")) {
            thisDay = "Hôm nay thứ tư, tặng tất cả khách hàng một phần trà sữa";
        } else if (thisDay.equals("Thu")) {
            thisDay = "Hôm nay thứ năm, giảm giá 20%";
        } else if (thisDay.equals("Fri")) {
            thisDay = "Hôm nay thứ sáu, freeship trong nội thành HN";
        } else if (thisDay.equals("Sat")) {
            thisDay = "Hôm nay thứ bảy, giảm giá 5%";
        } else {
            thisDay = "Hôm nay thứ chủ nhật, tặng voucher 10%";
        }

        return thisDay;
    }
}
