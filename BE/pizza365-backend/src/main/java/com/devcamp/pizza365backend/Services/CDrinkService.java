package com.devcamp.pizza365backend.Services;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.stereotype.Service;

import com.devcamp.pizza365backend.Models.CDrink;

@Service
public class CDrinkService {
     private CDrink drink1 = new CDrink("TRATAC", "Trà Tắc", 10000, null, new Date().getTime());
    private CDrink drink2 = new CDrink("COCA", "Cocacola", 15000, null, 1615177934000L);
    private CDrink drink3 = new CDrink("PEPSI", "Pepsi", 15000, null, 1615177934000L);
    private CDrink drink4 = new CDrink("LAVIE", "Lavie", 5000, null, 1615177934000L);
    private CDrink drink5 = new CDrink("TRASUA", "Trà Sữa Trân Châu", 40000, null, 1615177934000L);
    private CDrink drink6 = new CDrink("FANTA", "Fanta", 15000, null, 1615177934000L);
    private CDrink drink7 = new CDrink("CACAO", "Ca Cao Nóng", 15000, null, 1615177934000L);

    public ArrayList<CDrink> getAllDrinks(){

        ArrayList<CDrink> DrinksList = new ArrayList<>();
        DrinksList.add(drink1);
        DrinksList.add(drink2);
        DrinksList.add(drink3);
        DrinksList.add(drink4);
        DrinksList.add(drink5);
        DrinksList.add(drink6);
        DrinksList.add(drink7);

        return DrinksList;
    }
}
