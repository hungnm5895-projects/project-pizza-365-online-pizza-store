package com.devcamp.pizza365backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Pizza365BackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(Pizza365BackendApplication.class, args);
	}

}
