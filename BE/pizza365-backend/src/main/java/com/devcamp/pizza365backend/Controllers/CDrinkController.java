package com.devcamp.pizza365backend.Controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365backend.Models.CDrink;
import com.devcamp.pizza365backend.Services.CDrinkService;

@RestController
@CrossOrigin
public class CDrinkController {
    @Autowired
    CDrinkService cDrinkService;

    @GetMapping("/drinks")
    public ArrayList<CDrink> getDrinkList(){
        ArrayList<CDrink> drinksList = cDrinkService.getAllDrinks();
        return drinksList;
    }
}
